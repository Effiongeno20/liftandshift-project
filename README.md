### ABOUT THIS PROJECT ###
Multi Tier Web Application Stack
Host And Run on AWS Cloud For Production 
We Will be Using Lift And Shift Strategy

### SCENARIO ###
1)Application services runining on physical/virtual machings e.g Nginx,Tomcat,Maven etc.
2)Work loads in your Datacenter
3)Multiple teams runing varieties of servers e.g Virtualization team, DC OPS Team, Mornitoring Team, Sys Admin etc. 

### PROBLEM FACE USING THIS WAY OF DEPLOYMENT ON DATACENTER ###
1)Complex Management 
2)Scale Up/Down complexity
3)UpFront CapEx and Regular OpEx
4)Manual Process
5)Difficult to automate
6)Time Consuming

### SOLUTION TO THE PROBLEM MENTION ABOVE ###
To solve this problem we set up CLOUD INFRASTRUCTURE:
a)PayAsUgo Model
b)IAAS
c)Flexibility
d)Ease of Infra Mnanagement
e)Automation

### AWS SERVICES USE FOR THIS PROJECT IS LISTED BELOW ###
EC2 INSTANCES: Use for provisioning VM.
ELASTIC LOAD BALANCER: Nginx LB Replacement.
AUTOSCALING: Automation for VM scaling.
S3/EFS STORAGE: Shared storage. 
ROUT53: Private DNS service.
We will also be using AWS Services Like EBS, IAM, SG, ACM.

### FLOW OF EXECUTION ON THIS PROJECT ###
Login to AWS Account
Create key Pairs and Security Group
Lounch instances with user data [BASH SCRIPT
Update IP  to name mapping in route 53
Build Applicationg from source code
Upload to S3 Bucket 
Download build Artifact to Tomcat Ec2 instance
Setup ELB with HTTPS [ACM]
Map ELB Endpoint to website name in Rout53 DNS
Verify 
Build Autoscaling Group for Tomcat Instances .